import tensorflow as tf
import numpy as np

useDataSet1 = False
if (useDataSet1 == True):
    print("Small dataset being used ..... ")
    training_dataset = np.genfromtxt('data/normalized-N-shrunken-centroids-train.csv',
                      skip_header=1,
                      filling_values=-999)
    #read the training dataset This is a 71 by 3 matrix
    test_dataset = np.genfromtxt('data/normalized-N-shrunken-centroids-test.csv',
                          skip_header=1,
                          filling_values=-999)
if (useDataSet1 == False):
    print("Small dataset being used ..... ")
    training_dataset = np.genfromtxt('data/feature/128-signature/sample-cpg-train-cases-dim128-case10.txt',
                      skip_header=1,
                      filling_values=-999)
    #read the training dataset This is a 71 by 3 matrix
    test_dataset = np.genfromtxt('data/feature/128-signature/sample-cpg-test-cases-dim128-case10.txt',
                          skip_header=1,
                          filling_values=-999)

print("Test input size = ",test_dataset.shape)
print("Training input size = ",training_dataset .shape)

#train datasets
num_dimensions = 128 #make sure the output is not included
x_dataset = training_dataset[:,1:num_dimensions+1]
num_train_egs = x_dataset[:,1].size
num_columns = training_dataset[1,:].size
y_dataset=training_dataset[:,num_columns-2:num_columns]
print("X-Train", x_dataset)
print("Y-Train: " , y_dataset)

#test datasets
x_dataset_test = test_dataset[:,1:num_dimensions+1]
y_dataset_test = test_dataset[:,num_columns-2:num_columns]
print("X-Test: " , x_dataset_test)
print("Y-Test: " , y_dataset_test)

# Compute the dimensions
num_features = x_dataset[1,:].size
num_examples = x_dataset[:,1].size
num_predictions = y_dataset[1,:].size
print("features=", num_features , " egs=" , num_examples , " pred=" , num_predictions)
print("------------------------------------------------------------------------------")

# Parameters
num_tries_per_model = 3
regularization = [1000]
#regularization = [0.001]
beta = 1 # overridden at each iteration regularization
learning_rate = 0.001
training_epochs = 20001
batch_size = 100
display_step = 5000
logs_path = '/Users/19AyushA/Desktop/machinelearning/tensorflow/allergy/logs/neuralnets/mlp'

# Network Parameters
n_hidden_1 = 10 # 1st layer num features
n_hidden_2 = 10 # 2nd layer num features
n_input = num_features # input
n_classes = num_predictions # output

# tf Graph input
x = tf.placeholder("float", [None, n_input])
y = tf.placeholder("float", [None, n_classes])

# Create model
def multilayer_perceptron(_X, _weights, _biases):
    layer_1 = tf.nn.relu(tf.add(tf.matmul(_X, _weights['h1']), _biases['b1'])) #Hidden layer with RELU activation
    layer_2 = tf.nn.relu(tf.add(tf.matmul(layer_1, _weights['h2']), _biases['b2'])) #Hidden layer with RELU activation
    output = tf.matmul(layer_2, _weights['out']) + _biases['out']
    return output

    #relu_layer= tf.nn.relu(output)
    # Dropout on hidden layer: RELU layer
    #keep_prob = tf.placeholder("float")
    #relu_layer_dropout = tf.nn.dropout(relu_layer, keep_prob)

# Store layers weight & bias
weights = {
    'h1': tf.Variable(tf.random_normal([n_input, n_hidden_1])),
    'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'out': tf.Variable(tf.random_normal([n_hidden_2, n_classes]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'out': tf.Variable(tf.random_normal([n_classes]))
}

# Construct model
pred = multilayer_perceptron(x, weights, biases)

# Define loss and optimizer
#cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y)) # Softmax loss
cost = tf.nn.softmax_cross_entropy_with_logits( logits=pred, labels=y)
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost) # Adam Optimizer

# Initializing the variables
init = tf.initialize_all_variables()

# Launch the graph
with tf.Session() as sess:
    sess.run(init)

    avg_cost = 0.;
    # Training cycle
    for epoch in range(training_epochs):
        # Fit training
        sess.run(optimizer, feed_dict={x: x_dataset, y: y_dataset})
        # Compute average loss
        avg_cost = sess.run(cost, feed_dict={x: x_dataset, y: y_dataset})
        avg_cost_test = sess.run(cost, feed_dict={x: x_dataset_test, y: y_dataset_test})
        # Display logs per epoch step
        if epoch % display_step == 0:
            print ("Epoch:", (epoch+1), "cost=", avg_cost)

    print ("Optimization Finished! -- Cost=",avg_cost)

    # Test model
    correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
    # Calculate accuracy
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    print ("Accuracy Train:", accuracy.eval({x: x_dataset, y: y_dataset}))
    print ("Accuracy Test:", accuracy.eval({x: x_dataset_test, y: y_dataset_test}))
